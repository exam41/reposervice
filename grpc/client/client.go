package client

import (
	"reposervice/config"
	pb "reposervice/genproto/payment_service"
	"reposervice/genproto/repo_service"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	Category() repo_service.CategoryServiceClient
	Product() repo_service.ProductServiceClient
	Income() repo_service.IncomeServiceClient
	IncomeProduct() repo_service.IncomeProductServiceClient
	Storage() repo_service.StorageServiceClient

	Courier() pb.CourierServiceClient
}

type grpcClients struct {
	category       repo_service.CategoryServiceClient
	product        repo_service.ProductServiceClient
	income         repo_service.IncomeServiceClient
	income_product repo_service.IncomeProductServiceClient
	storage        repo_service.StorageServiceClient
	courier        pb.CourierServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connRepoService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	connPayService, err := grpc.Dial(
		cfg.PayServiceGrpcHost+cfg.PayServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err!= nil {
        return nil, err
    }
	return &grpcClients{
		category:       repo_service.NewCategoryServiceClient(connRepoService),
		product:        repo_service.NewProductServiceClient(connRepoService),
		income:         repo_service.NewIncomeServiceClient(connRepoService),
		income_product: repo_service.NewIncomeProductServiceClient(connRepoService),
		storage:        repo_service.NewStorageServiceClient(connRepoService),
		courier:        pb.NewCourierServiceClient(connPayService),
	}, nil
}

func (g *grpcClients) Category() repo_service.CategoryServiceClient {
	return g.category
}

func (g *grpcClients) Product() repo_service.ProductServiceClient {
	return g.product
}

func (g *grpcClients) Income() repo_service.IncomeServiceClient {
	return g.income
}

func (g *grpcClients) IncomeProduct() repo_service.IncomeProductServiceClient {
	return g.income_product
}

func (g *grpcClients) Storage() repo_service.StorageServiceClient {
	return g.storage
}

func (g *grpcClients) Courier() pb.CourierServiceClient {
    return g.courier
}
