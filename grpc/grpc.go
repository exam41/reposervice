package grpc

import (
	pbr "reposervice/genproto/repo_service"
	"reposervice/grpc/client"
	"reposervice/pkg/logger"
	"reposervice/service"
	"reposervice/storage"

	"google.golang.org/grpc"
)

func SetUpServer(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *grpc.Server {
	grpcServer := grpc.NewServer()

	pbr.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(strg, services, log))
	pbr.RegisterProductServiceServer(grpcServer, service.NewProductService(strg, services, log))
	pbr.RegisterIncomeServiceServer(grpcServer, service.NewIncomeService(strg, services, log))
	pbr.RegisterIncomeProductServiceServer(grpcServer, service.NewIncomeProductService(strg, services, log))
	pbr.RegisterStorageServiceServer(grpcServer, service.NewStorageService(strg, services, log))

	return grpcServer
}
