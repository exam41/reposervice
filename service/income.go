package service

import (
	"context"
	"fmt"
	pbr "reposervice/genproto/repo_service"
	"reposervice/grpc/client"
	"reposervice/pkg/generate"
	"reposervice/pkg/logger"
	"reposervice/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type incomeService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewIncomeService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *incomeService {
	return &incomeService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (s *incomeService) Create(ctx context.Context, request *pbr.Income) (*pbr.Income, error) {
	id, err := s.storage.Income().Count(context.Background(), &emptypb.Empty{})
	if err != nil {
		request.Id = "P-00001"
	} else {
		request.Id = generate.GenerateNextID(id.Id)
	}

	return s.storage.Income().Create(ctx, request)

}

func (s *incomeService) Get(ctx context.Context, request *pbr.PrimaryKeyIncome) (*pbr.Income, error) {
	return s.storage.Income().Get(ctx, request)
}

func (s *incomeService) GetList(ctx context.Context, request *pbr.IncomeRequest) (*pbr.IncomeResponse, error) {
	return s.storage.Income().GetList(ctx, request)
}

func (s *incomeService) Update(ctx context.Context, request *pbr.Income) (*pbr.Income, error) {
	return s.storage.Income().Update(ctx, request)
}

func (s *incomeService) Delete(ctx context.Context, request *pbr.PrimaryKeyIncome) (*emptypb.Empty, error) {
	return s.storage.Income().Delete(ctx, request)
}

func (s *incomeService) Count(ctx context.Context, request *emptypb.Empty) (*pbr.PrimaryKeyIncome, error) {
	return s.storage.Income().Count(ctx, request)
}

func (s *incomeService) EndIncome(ctx context.Context, request *pbr.PrimaryKeyIncome) (*pbr.Income, error) {

	iincomeproduct, err := s.services.IncomeProduct().GetList(context.Background(), &pbr.IncomeProductRequest{Page: 1, Limit: 10})
	if err != nil {
		fmt.Println("Err", err)
	}
	storage, err := s.services.Storage().GetList(context.Background(), &pbr.StorageRequest{Page: 1, Limit: 10})
	if err != nil {
		fmt.Println("Error", err)
	}

	mapstorage := make(map[string]*pbr.Storage)
	for _, v := range storage.Storages {

		mapstorage[v.ProductId] = v

	}

	for _, v := range iincomeproduct.IncomeProducts {
		if v.ProductId == mapstorage[v.ProductId].ProductId {
			if v.IncomePrice != mapstorage[v.ProductId].IncomePrice {

				_, err := s.services.Product().Update(context.Background(), &pbr.Product{Price: v.IncomePrice})
				if err != nil {
					fmt.Println("error", err)
				}
				if mapstorage[v.ProductId].ProductId == v.ProductId {
					if mapstorage[v.ProductId].IncomePrice != v.IncomePrice {
						_, err = s.services.Storage().Update(context.Background(), &pbr.Storage{IncomePrice: v.IncomePrice, Quantity: v.Quantity})
						if err != nil {
							fmt.Println("Err", err)
						}

						_, err := s.services.Product().Update(context.Background(), &pbr.Product{Id: v.ProductId, Price: v.IncomePrice * v.Quantity, Name: v.Name, CategoryId: v.CategoryId})
						if err != nil {
							fmt.Println("Err", err)
						}
					}

				} else {
					_, err := s.services.Storage().Create(context.Background(), &pbr.CreateStorage{ProductId: v.Id, Quantity: v.Quantity, CategoryId: v.CategoryId, IncomePrice: v.IncomePrice, TotalPrice: v.Quantity * v.IncomePrice, Name: v.Name})
					if err != nil {
						fmt.Println("Err", err)
					}
					_, err = s.services.Product().Create(context.Background(), &pbr.CreateProduct{Price: v.IncomePrice * v.Quantity, Name: v.Name, CategoryId: v.CategoryId})
					if err != nil {
						fmt.Println("s")
					}

				}
				fmt.Println("s")

			}

		}
	}

	updatedincome, err := s.services.Income().Update(context.Background(), &pbr.Income{Id: request.Id, Status: "finished"})
	if err != nil {
		fmt.Println("Err", err)
	}
	return updatedincome, nil
}
