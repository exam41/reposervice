package service

import (
	"context"
	pbr "reposervice/genproto/repo_service"
	"reposervice/grpc/client"
	"reposervice/pkg/logger"
	"reposervice/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type categoryService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewCategoryService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *categoryService {
	return &categoryService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (s *categoryService) Create(ctx context.Context, request *pbr.CreateCategory) (*pbr.Category, error) {
	return s.storage.Category().Create(ctx, request)
}

func (s *categoryService) Get(ctx context.Context, request *pbr.PrimaryKeyCategory) (*pbr.Category, error) {
	return s.storage.Category().Get(ctx, request)
}

func (s *categoryService) GetList(ctx context.Context, request *pbr.CategoryRequest) (*pbr.CategoryRespose, error) {
	return s.storage.Category().GetList(ctx, request)
}

func (s *categoryService) Update(ctx context.Context, request *pbr.Category) (*pbr.Category, error) {
	return s.storage.Category().Update(ctx, request)
}

func (s *categoryService) Delete(ctx context.Context, request *pbr.PrimaryKeyCategory) (*emptypb.Empty, error) {
	return s.storage.Category().Delete(ctx, request)
}
