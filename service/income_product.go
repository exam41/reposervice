package service

import (
	"context"
	pbr "reposervice/genproto/repo_service"
	"reposervice/grpc/client"
	"reposervice/pkg/barcode"
	"reposervice/pkg/logger"
	"reposervice/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type incomeProductService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewIncomeProductService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *incomeProductService {
	return &incomeProductService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (s *incomeProductService) Create(ctx context.Context, request *pbr.IncomeProduct) (*pbr.IncomeProduct, error) {
	request.Barcode = barcode.BarcodeGenerate()
	return s.storage.IncomeProduct().Create(ctx, request)
}

func (s *incomeProductService) Get(ctx context.Context, request *pbr.PrimaryKeyIncomeProduct) (*pbr.IncomeProduct, error) {
	return s.storage.IncomeProduct().Get(ctx, request)
}

func (s *incomeProductService) GetList(ctx context.Context, request *pbr.IncomeProductRequest) (*pbr.IncomeProductResponse, error) {
	return s.storage.IncomeProduct().GetList(ctx, request)
}

func (s *incomeProductService) Update(ctx context.Context, request *pbr.IncomeProduct) (*pbr.IncomeProduct, error) {
	return s.storage.IncomeProduct().Update(ctx, request)
}

func (s *incomeProductService) Delete(ctx context.Context, request *pbr.PrimaryKeyIncomeProduct) (*emptypb.Empty, error) {
	return s.storage.IncomeProduct().Delete(ctx, request)
}

func (s *incomeProductService) Count(ctx context.Context, request *emptypb.Empty) (*pbr.PrimaryKeyIncomeProduct, error) {
	return s.storage.IncomeProduct().Count(ctx, request)
}
