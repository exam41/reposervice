package service

import (
	"context"
	pbr "reposervice/genproto/repo_service"
	"reposervice/grpc/client"
	"reposervice/pkg/barcode"
	"reposervice/pkg/logger"
	"reposervice/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type storageService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewStorageService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *storageService {
	return &storageService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (s *storageService) Create(ctx context.Context, request *pbr.CreateStorage) (*pbr.Storage, error) {
	request.Barcode = barcode.BarcodeGenerate()
	return s.storage.Storage().Create(ctx, request)
}

func (s *storageService) Get(ctx context.Context, request *pbr.PrimaryKeyStorage) (*pbr.Storage, error) {
	return s.storage.Storage().Get(ctx, request)
}

func (s *storageService) GetList(ctx context.Context, request *pbr.StorageRequest) (*pbr.StorageResponse, error) {
	return s.storage.Storage().GetList(ctx, request)
}

func (s *storageService) Update(ctx context.Context, request *pbr.Storage) (*pbr.Storage, error) {
	return s.storage.Storage().Update(ctx, request)
}

func (s *storageService) Delete(ctx context.Context, request *pbr.PrimaryKeyStorage) (*emptypb.Empty, error) {
	return s.storage.Storage().Delete(ctx, request)
}
