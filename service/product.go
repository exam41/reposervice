package service

import (
	"context"
	pbr "reposervice/genproto/repo_service"
	"reposervice/grpc/client"
	"reposervice/pkg/barcode"
	"reposervice/pkg/logger"
	"reposervice/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type productService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewProductService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *productService {
	return &productService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (s *productService) Create(ctx context.Context, request *pbr.CreateProduct) (*pbr.Product, error) {
	request.Barcode = barcode.BarcodeGenerate()
	return s.storage.Product().Create(ctx, request)
}

func (s *productService) Get(ctx context.Context, request *pbr.PrimaryKeyProduct) (*pbr.Product, error) {
	return s.storage.Product().Get(ctx, request)
}

func (s *productService) GetList(ctx context.Context, request *pbr.ProductRequest) (*pbr.PoroductResponse, error) {
	return s.storage.Product().GetList(ctx, request)
}

func (s *productService) Update(ctx context.Context, request *pbr.Product) (*pbr.Product, error) {
	return s.storage.Product().Update(ctx, request)
}

func (s *productService) Delete(ctx context.Context, request *pbr.PrimaryKeyProduct) (*emptypb.Empty, error) {
	return s.storage.Product().Delete(ctx, request)
}
