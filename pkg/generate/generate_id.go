package generate

import (
	"fmt"
	"strconv"
)

// func GenerateExternalID(id string, name string) string {
// 	a:=name[1:]
//     b:= id[2:]
// 	idstr, err := strconv.Atoi(b)
// 	if err != nil {
// 		fmt.Println("errr", err.Error())
// 	}

// 	idstr++
// 	id = fmt.Sprintf("%s-%04d", a, idstr)

// 	return id
// }

func GenerateNextID(lastID string) string {
	lastNumStr := lastID[2:] // Extract the numeric part of the last ID
	lastNum, _ := strconv.Atoi(lastNumStr)
	nextNum := lastNum + 1
	nextID := fmt.Sprintf("P-%05d", nextNum) // Pad the number with zeros
	return nextID
}
