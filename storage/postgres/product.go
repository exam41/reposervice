package postgres

import (
	"context"
	"fmt"
	"reposervice/pkg/helper"
	"reposervice/pkg/logger"
	"reposervice/storage"

	pbr "reposervice/genproto/repo_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type productRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewProductRepo(db *pgxpool.Pool, log logger.ILogger) storage.IProductStorage {
	return &productRepo{
		db:  db,
		log: log,
	}
}

func (r *productRepo) Create(ctx context.Context, request *pbr.CreateProduct) (*pbr.Product, error) {
	product := pbr.Product{}

	query := `insert into products (id, name, category_id,barcode, price, searching_column) values ($1, $2, $3,$4,$5,$6) returning id, name, category_id,barcode, price`

	searchingColumn := fmt.Sprintf("%s %s %s %d", request.GetName(), request.GetCategoryId(), request.GetBarcode(), request.GetPrice())
	if err := r.db.QueryRow(ctx, query, uuid.New().String(), request.GetName(), request.GetCategoryId(), request.GetBarcode(), request.GetPrice(), searchingColumn).Scan(
		&product.Id,
		&product.Name,
		&product.CategoryId,
		&product.Barcode,
		&product.Price,
	); err != nil {
		//r.log.Error("Error creating product", logger.Error(err))
		fmt.Println("err",err)
	}
	return &product, nil
}

func (r *productRepo) Get(ctx context.Context, request *pbr.PrimaryKeyProduct) (*pbr.Product, error) {

	product := pbr.Product{}
	query := `select id, name, category_id,barcode, price from products where id = $1`

	err := r.db.QueryRow(ctx, query, request.GetId()).Scan(&product.Id, &product.Name, &product.CategoryId, &product.Barcode, &product.Price)
	if err != nil {
		r.log.Error("Error getting product", logger.Error(err))
	}
	return &product, nil

}

func (r *productRepo) GetList(ctx context.Context, request *pbr.ProductRequest) (*pbr.PoroductResponse, error) {

	var (
		resp   = pbr.PoroductResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}

	countQuery := ` select count(*) from products `
	if err := r.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		r.log.Error("error counting category", logger.Error(err))
	}

	query := `select id, name, category_id,barcode, price from products ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())
	rows, err := r.db.Query(ctx, query)
	if err != nil {
		r.log.Error("Error getting products", logger.Error(err))
	}
	defer rows.Close()
	for rows.Next() {
		product := pbr.Product{}
		if err := rows.Scan(&product.Id, &product.Name, &product.CategoryId, &product.Barcode, &product.Price); err != nil {
			r.log.Error("Error getting products", logger.Error(err))
		}
		resp.Products = append(resp.Products, &product)
	}
	resp.Count = count
	return &resp, nil
}

func (r *productRepo) Update(ctx context.Context, request *pbr.Product) (*pbr.Product, error) {

	var (
		products = pbr.Product{}
		params   = make(map[string]interface{})
		query    = ` update products set  `
		filter   = ""
	)

	params["id"] = request.GetId()
	fmt.Println("Products point id", request.GetId())

	if request.GetName() != "" {
		params["name"] = request.GetName()
		filter += " name= @name,"
	}
	if request.GetCategoryId() != "" {
		params["category_id"] = request.GetCategoryId()
		filter += " category_id= @category_id,"
	}
	if request.GetBarcode() != "" {
		params["barcode"] = request.GetBarcode()
		filter += " barcode= @barcode,"
	}
	if request.GetPrice() != 0 {
		params["price"] = request.GetPrice()
		filter += " price= @price,"
	}
	query += filter + ` updated_at=now() where deleted_at=0 and id=@id returning id, name, category_id,barcode, price`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	err := r.db.QueryRow(ctx, fullQuery, args...).Scan(&products.Id, &products.Name, &products.CategoryId, &products.Barcode, &products.Price)
	if err != nil {
		r.log.Error("Error updating product in postgres", logger.Error(err))
	}
	return &products, nil

}

func (r *productRepo) Delete(ctx context.Context, request *pbr.PrimaryKeyProduct) (*emptypb.Empty, error) {
	query := ` update products set deleted_at = extract(epoch from current_timestamp) where id = $1`
    _, err := r.db.Exec(ctx, query, request.GetId())
    return nil, err
}

