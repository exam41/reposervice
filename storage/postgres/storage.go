package postgres

import (
	"context"
	"fmt"
	"reposervice/pkg/helper"
	"reposervice/pkg/logger"
	"reposervice/storage"

	pbr "reposervice/genproto/repo_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type storageRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewStorageRepo(db *pgxpool.Pool, log logger.ILogger) storage.IStorageStorage {
	return &storageRepo{
		db:  db,
		log: log,
	}
}

func (s *storageRepo) Create(ctx context.Context, request *pbr.CreateStorage) (*pbr.Storage, error) {
	storage := pbr.Storage{}
	query := ` insert into storage(id,sale_point_id,product_id,barcode,income_price,quantity,total_price,searching_column) values($1,$2,$3,$4,$5,$6,$7,$8)
	     returning id,sale_point_id,product_id,barcode,income_price,quantity,total_price`
	searchingColumn := fmt.Sprintf("%s %s %s %d ", request.GetSalePointId(), request.GetProductId(), request.GetBarcode(), request.GetIncomePrice())
	err := s.db.QueryRow(ctx, query, uuid.New().String(), request.GetSalePointId(), request.GetProductId(), request.GetBarcode(), request.GetIncomePrice(), request.GetQuantity(), request.GetTotalPrice(), searchingColumn).Scan(&storage.Id, &storage.SalePointId, &storage.ProductId, &storage.Barcode, &storage.IncomePrice, &storage.Quantity, &storage.TotalPrice)
	if err != nil {
		//s.log.Error("Error creating storage", logger.Error(err))
		fmt.Println(err)
	}
	return &storage, nil
}

func (s *storageRepo) Get(ctx context.Context, request *pbr.PrimaryKeyStorage) (*pbr.Storage, error) {
	storage := pbr.Storage{}
	query := `select id,sale_point_id,product_id,barcode,income_price,quantity,total_price from storage where id = $1`
	err := s.db.QueryRow(ctx, query, request.GetId()).Scan(&storage.Id, &storage.SalePointId, &storage.ProductId, &storage.Barcode, &storage.IncomePrice, &storage.Quantity, &storage.TotalPrice)
	if err != nil {
		//s.log.Error("Error getting storage", logger.Error(err))
		fmt.Println("Err")
	}
	return &storage, nil
}

func (s *storageRepo) GetList(ctx context.Context, request *pbr.StorageRequest) (*pbr.StorageResponse, error) {
	var (
		resp   = pbr.StorageResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := ` select count(*) from storage ` + filter
	if err := s.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		s.log.Error("error counting storage", logger.Error(err))
	}
	query := `select id,sale_point_id,product_id,barcode,income_price,quantity,total_price from storage ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := s.db.Query(ctx, query)
	if err != nil {
		s.log.Error("Error getting storages", logger.Error(err))
	}
	defer rows.Close()
	for rows.Next() {
		storage := pbr.Storage{}
		if err := rows.Scan(&storage.Id, &storage.SalePointId, &storage.ProductId, &storage.Barcode, &storage.IncomePrice, &storage.Quantity, &storage.TotalPrice); err != nil {
			s.log.Error("Error getting storages", logger.Error(err))
		}
		resp.Storages = append(resp.Storages, &storage)
	}
	resp.Count = count
	return &resp, nil
}

func (s *storageRepo) Update(ctx context.Context, request *pbr.Storage) (*pbr.Storage, error) {
	var (
		storage = pbr.Storage{}
		params  = make(map[string]interface{})
		query   = ` update storage set  `
		filter  = ""
	)

	params["id"] = request.GetId()
	if request.GetSalePointId() != "" {
		params["sale_point"] = request.GetSalePointId()
		filter += " sale_point= @sale_point,"
	}
	if request.GetProductId() != "" {
		params["product_id"] = request.GetProductId()
		filter += " product_id= @product_id,"
	}
	if request.GetBarcode() != "" {
		params["barcode"] = request.GetBarcode()
		filter += " barcode= @barcode,"
	}
	if request.GetIncomePrice() != 0 {
		params["income_price"] = request.GetIncomePrice()
		filter += " income_price= @income_price,"
	}
	if request.GetQuantity() != 0 {
		params["quantity"] = request.GetQuantity()
		filter += " quantity= @quantity,"
	}
	if request.GetTotalPrice() != 0 {
		params["total_price"] = request.GetTotalPrice()
		filter += " total_price= @total_price,"
	}
	query += filter + ` updated_at=now() where deleted_at=0 and id=@id returning id,sale_point,product_id,barcode,income_price,quantity,total_price`
	fullQuery, args := helper.ReplaceQueryParams(query, params)
	err := s.db.QueryRow(ctx, fullQuery, args...).Scan(&storage.Id, &storage.SalePointId, &storage.ProductId, &storage.Barcode, &storage.IncomePrice, &storage.Quantity, &storage.TotalPrice)
	if err != nil {
		s.log.Error("Error updating storage", logger.Error(err))
	}
	return &storage, nil
}

func (s *storageRepo) Delete(ctx context.Context, request *pbr.PrimaryKeyStorage) (*emptypb.Empty, error) {
	query := ` update storage set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := s.db.Exec(ctx, query, request.GetId())
	return nil, err
}
