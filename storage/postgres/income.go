package postgres

import (
	"context"
	"errors"
	"fmt"
	"reposervice/pkg/helper"
	"reposervice/pkg/logger"
	"reposervice/storage"

	pbr "reposervice/genproto/repo_service"

	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type incomeRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewIncomeRepo(db *pgxpool.Pool, log logger.ILogger) storage.IIncomeStorage {
	return &incomeRepo{
		db:  db,
		log: log,
	}
}

func (r *incomeRepo) Create(ctx context.Context, request *pbr.Income) (*pbr.Income, error) {
	income := pbr.Income{}

	query := `insert into incomes (id, courier_id, status, searching_column) values ($1, $2, $3,$4) returning id, courier_id, status`

	searchingColumn := fmt.Sprintf("%s %s", request.GetCourierId(), request.GetStatus())

	if err := r.db.QueryRow(ctx, query, request.Id, request.GetCourierId(), request.GetStatus(), searchingColumn).Scan(
		&income.Id,
		&income.CourierId,
		&income.Status,
	); err != nil {
		//r.log.Error("Error creating income", logger.Error(err))

		fmt.Println("idd",income.Id)
		fmt.Println("error is here in postgres", err)
	}
	fmt.Println("id", request.Id)
	return &income, nil
}

func (r *incomeRepo) Get(ctx context.Context, request *pbr.PrimaryKeyIncome) (*pbr.Income, error) {
	income := pbr.Income{}
	query := `select id, courier_id, status from incomes where id = $1`

	err := r.db.QueryRow(ctx, query, request.GetId()).Scan(&income.Id, &income.CourierId, &income.Status)
	if err != nil {
		
		r.log.Error("Error getting income", logger.Error(err))
	}
	return &income, nil
}

func (r *incomeRepo) GetList(ctx context.Context, request *pbr.IncomeRequest) (*pbr.IncomeResponse, error) {
	var (
		resp   = pbr.IncomeResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := ` select count(*) from incomes ` + filter
	if err := r.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		r.log.Error("error counting income", logger.Error(err))
	}

	query := `select id, courier_id, status from incomes ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())
	rows, err := r.db.Query(ctx, query)
	if err != nil {
		r.log.Error("Error getting incomes", logger.Error(err))
	}
	defer rows.Close()
	for rows.Next() {
		income := pbr.Income{}
		if err := rows.Scan(&income.Id, &income.CourierId, &income.Status); err != nil {
			r.log.Error("Error getting incomes", logger.Error(err))
		}
		resp.Incomes = append(resp.Incomes, &income)
	}
	resp.Count = count
	return &resp, nil
}

func (r *incomeRepo) Update(ctx context.Context, request *pbr.Income) (*pbr.Income, error) {

	var (
		income = pbr.Income{}
		params = make(map[string]interface{})
		query  = ` update incomes set  `
	)
	params["id"] = request.GetId()

	if request.GetCourierId() != "" {
		params["courier_id"] = request.GetCourierId()
		query += " courier_id= @courier_id,"
	}
	if request.GetStatus() != "" {
		params["status"] = request.GetStatus()
		query += " status= @status,"
	}
	query += ` updated_at=now() where deleted_at=0 and id=@id returning id, courier_id, status`
	fullQuery, args := helper.ReplaceQueryParams(query, params)
	if err := r.db.QueryRow(ctx, fullQuery, args...).Scan(&income.Id, &income.CourierId, &income.Status); err != nil {
		r.log.Error("Error updating income", logger.Error(err))
	}
	return &income, nil

}

func (r *incomeRepo) Delete(ctx context.Context, request *pbr.PrimaryKeyIncome) (*emptypb.Empty, error) {
	query := ` update incomes set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := r.db.Exec(ctx, query, request.GetId())
	return nil, err
}

func (r *incomeRepo) Count(ctx context.Context, empty *emptypb.Empty) (*pbr.PrimaryKeyIncome, error) {
	count := pbr.PrimaryKeyIncome{}

	query := "SELECT id FROM incomes ORDER BY id DESC LIMIT 1"

	var id string
	err := r.db.QueryRow(ctx, query).Scan(&id)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			id = "P-00001" // Set a default ID when no rows are returned
		} else {
			return nil, err
		}
	}

	count.Id = id
	return &count, nil
}

