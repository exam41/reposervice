package postgres

import (
	"context"
	"fmt"
	"reposervice/pkg/helper"
	"reposervice/pkg/logger"
	"reposervice/storage"

	pbr "reposervice/genproto/repo_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type categoryRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewCategoryRepo(db *pgxpool.Pool, log logger.ILogger) storage.ICategoryStorage {
	return &categoryRepo{
		db:  db,
		log: log,
	}
}

func (r *categoryRepo) Create(ctx context.Context, request *pbr.CreateCategory) (*pbr.Category, error) {

	category := pbr.Category{}

	query := ` insert into categories(id, name, parent_id,searching_column) values($1,$2,$3,$4)
	     returning id, name, parent_id`

	searchingColumn := fmt.Sprintf("%s %s", request.GetName(), request.GetParentId())

	uid:=uuid.New().String()
	request.ParentId=uid
	err := r.db.QueryRow(ctx, query, uid,request.GetName(), request.ParentId, searchingColumn).Scan(&category.Id, &category.Name, &category.ParentId)
	if err != nil {
		//r.log.Error("Error creating category", logger.Error(err))

		fmt.Println("Er",err)
	}

	return &category, nil
}

func (r *categoryRepo) Get(ctx context.Context, request *pbr.PrimaryKeyCategory) (*pbr.Category, error) {
	category := pbr.Category{}

	query := `select id, name, parent_id from categories where id=$1`

	err := r.db.QueryRow(ctx, query, request.Id).Scan(&category.Id, &category.Name, &category.ParentId)
	if err != nil {
		//r.log.Error("Error getting category", logger.Error(err))
		fmt.Println("err",err)
	}

	return &category, nil
}

func (r *categoryRepo) GetList(ctx context.Context, request *pbr.CategoryRequest) (*pbr.CategoryRespose, error) {

	var (
		resp   = pbr.CategoryRespose{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := ` select count(*) from categories ` + filter
	if err := r.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		//r.log.Error("error counting category", logger.Error(err))
		fmt.Println("Err",err)
	}

	query := `select id, name, parent_id from categories ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		r.log.Error("error while getting category in postgres", logger.Error(err))
	}
	defer rows.Close()

	for rows.Next() {
		category := pbr.Category{}
		if err := rows.Scan(&category.Id, &category.Name, &category.ParentId); err != nil {
			r.log.Error("error while getting category in postgres", logger.Error(err))
		}
		resp.Categories = append(resp.Categories, &category)
	}
	resp.Count = count

	return &resp, nil

}
func (r *categoryRepo) Update(ctx context.Context, request *pbr.Category) (*pbr.Category, error) {

	var (
		categories = pbr.Category{}
		params     = make(map[string]interface{})
		query      = ` update categories set  `
		filter     = ""
	)

	params["id"] = request.GetId()
	fmt.Println("Categories point id", request.GetId())

	if request.GetParentId() != "" {
		params["parentId"] = request.GetParentId()
		filter += " parent_id= @parent_id,"
	}
	if request.GetName() != "" {
		params["name"] = request.GetName()
		filter += " name= @name,"
	}

	query += filter + ` updated_at=now() where deleted_at=0 and id=@id returning id, name, parent_id`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := r.db.QueryRow(ctx, fullQuery, args...).Scan(
		&categories.Id,
		&categories.Name,
		&categories.ParentId,
	); err != nil {
		r.log.Error("Error updating category", logger.Error(err))
	}

	return &categories, nil

}

func (r *categoryRepo) Delete(ctx context.Context, request *pbr.PrimaryKeyCategory) (*emptypb.Empty, error) {
	query := ` update categories set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := r.db.Exec(ctx, query, request.Id)
	return nil, err
}
