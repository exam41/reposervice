package postgres

import (
	"context"
	"fmt"
	"reposervice/pkg/helper"
	"reposervice/pkg/logger"
	"reposervice/storage"

	pbr "reposervice/genproto/repo_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type incomeProductRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewIncomeProductRepo(db *pgxpool.Pool, log logger.ILogger) storage.IIncomeProductStorage {
	return &incomeProductRepo{
		db:  db,
		log: log,
	}
}

func (r *incomeProductRepo) Create(ctx context.Context, request *pbr.IncomeProduct) (*pbr.IncomeProduct, error) {
	incomeproduct := pbr.IncomeProduct{}
	query := `insert into income_products (id, income_id,category_id,product_id,product_name,barcode,quantity, income_price,searching_column) values ($1, $2, $3,$4,$5,$6,$7,$8,$9)
	   returning id, income_id, category_id, product_id, product_name, barcode, quantity, income_price`

	searchingColumn := fmt.Sprintf("%s %s %s %s %d %d %s", request.GetIncomeId(), request.GetCategoryId(), request.GetProductId(), request.GetName(), request.GetQuantity(), request.GetIncomePrice(), request.GetBarcode())

	uid := uuid.New().String()
	err := r.db.QueryRow(ctx, query, uid, request.GetIncomeId(), request.GetCategoryId(), request.GetProductId(), request.GetName(), request.GetBarcode(), request.GetQuantity(), request.GetIncomePrice(), searchingColumn).Scan(
		&incomeproduct.Id,
		&incomeproduct.IncomeId,
		&incomeproduct.CategoryId,
		&incomeproduct.ProductId,
		&incomeproduct.Name,
		&incomeproduct.Barcode,
		&incomeproduct.Quantity,
		&incomeproduct.IncomePrice,
	)
	if err != nil {
		//r.log.Error("Error creating income product", logger.Error(err))

		fmt.Println("Err", err)
	}
	return &incomeproduct, nil
}

func (r *incomeProductRepo) Get(ctx context.Context, request *pbr.PrimaryKeyIncomeProduct) (*pbr.IncomeProduct, error) {

	incomeproduct := pbr.IncomeProduct{}

	query := `select id, income_id, category_id, product_id, product_name, barcode, quantity, income_price from income_products where id = $1`

	err := r.db.QueryRow(ctx, query, request.GetId()).Scan(
		&incomeproduct.Id,
		&incomeproduct.IncomeId,
		&incomeproduct.CategoryId,
		&incomeproduct.ProductId,
		&incomeproduct.Name,
		&incomeproduct.Barcode,
		&incomeproduct.Quantity,
		&incomeproduct.IncomePrice,
	)
	if err != nil {
		r.log.Error("Error getting income product", logger.Error(err))
	}
	return &incomeproduct, nil
}

func (r *incomeProductRepo) GetList(ctx context.Context, request *pbr.IncomeProductRequest) (*pbr.IncomeProductResponse, error) {

	var (
		resp   = pbr.IncomeProductResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := ` select count(*) from income_products ` + filter
	if err := r.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		r.log.Error("error counting income", logger.Error(err))
	}
	query := `select id, income_id, category_id, product_id, product_name, barcode, quantity, income_price from income_products ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())
	rows, err := r.db.Query(ctx, query)
	if err != nil {
		r.log.Error("Error getting incomes", logger.Error(err))
	}
	defer rows.Close()
	for rows.Next() {
		incomeproduct := pbr.IncomeProduct{}
		if err := rows.Scan(&incomeproduct.Id, &incomeproduct.IncomeId, &incomeproduct.CategoryId, &incomeproduct.ProductId, &incomeproduct.Name, &incomeproduct.Barcode, &incomeproduct.Quantity, &incomeproduct.IncomePrice); err != nil {
			r.log.Error("Error getting incomes", logger.Error(err))
		}
		resp.IncomeProducts = append(resp.IncomeProducts, &incomeproduct)
	}
	resp.Count = count
	return &resp, nil
}

func (r *incomeProductRepo) Update(ctx context.Context, request *pbr.IncomeProduct) (*pbr.IncomeProduct, error) {
	var (
		incomeproduct = pbr.IncomeProduct{}
		params        = make(map[string]interface{})
		query         = ` update income_products set  `
		filter        = ""
	)
	params["id"] = request.GetId()
	if request.GetIncomeId() != "" {
		params["income_id"] = request.GetIncomeId()
		filter += " income_id= @income_id,"
	}
	if request.GetCategoryId() != "" {
		params["category_id"] = request.GetCategoryId()
		filter += " category_id= @category_id,"
	}
	if request.GetProductId() != "" {
		params["product_id"] = request.GetProductId()
		filter += " product_id= @product_id,"
	}
	if request.GetName() != "" {
		params["product_name"] = request.GetName()
		filter += " product_name= @product_name,"
	}
	if request.GetBarcode() != "" {
		params["barcode"] = request.GetBarcode()
		filter += " barcode= @barcode,"
	}
	if request.GetQuantity() != 0 {
		params["quantity"] = request.GetQuantity()
		filter += " quantity= @quantity,"
	}
	if request.GetIncomePrice() != 0 {
		params["income_price"] = request.GetIncomePrice()
		filter += " income_price= @income_price,"
	}

	query += filter + ` updated_at=now() where deleted_at=0 and id=@id returning id, income_id, category_id, product_id, product_name, barcode, quantity, income_price`
	fullQuery, args := helper.ReplaceQueryParams(query, params)
	err := r.db.QueryRow(ctx, fullQuery, args...).Scan(&incomeproduct.Id, &incomeproduct.IncomeId, &incomeproduct.CategoryId, &incomeproduct.ProductId, &incomeproduct.Name, &incomeproduct.Barcode, &incomeproduct.Quantity, &incomeproduct.IncomePrice)
	if err != nil {
		r.log.Error("Error updating income product", logger.Error(err))
	}
	return &incomeproduct, nil
}

func (r *incomeProductRepo) Delete(ctx context.Context, request *pbr.PrimaryKeyIncomeProduct) (*emptypb.Empty, error) {
	query := ` update income_products set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := r.db.Exec(ctx, query, request.GetId())
	return nil, err
}

func (r *incomeProductRepo) Count(ctx context.Context, empty *emptypb.Empty) (*pbr.PrimaryKeyIncomeProduct, error) {

	count := pbr.PrimaryKeyIncomeProduct{}

	query := `SELECT id FROM income_products ORDER BY id DESC LIMIT 1`

	row := r.db.QueryRow(ctx, query)
	var id string
	err := row.Scan(&id)
	if err != nil {
		r.log.Error("err", logger.Error(err))

	}
	count.Id = id
	return &count, nil
}
