package storage

import (
	"context"
	pbr "reposervice/genproto/repo_service"

	"google.golang.org/protobuf/types/known/emptypb"
)

type IStorage interface {
	Close()
	Category() ICategoryStorage
	Product() IProductStorage
	Income() IIncomeStorage
	IncomeProduct() IIncomeProductStorage
	Storage()    IStorageStorage
}

type ICategoryStorage interface {
	Create(context.Context, *pbr.CreateCategory) (*pbr.Category, error)
	Get(context.Context, *pbr.PrimaryKeyCategory) (*pbr.Category, error)
	GetList(context.Context, *pbr.CategoryRequest) (*pbr.CategoryRespose, error)
	Update(context.Context, *pbr.Category) (*pbr.Category, error)
	Delete(context.Context, *pbr.PrimaryKeyCategory) (*emptypb.Empty, error)
}
type IProductStorage interface {
	Create(context.Context, *pbr.CreateProduct) (*pbr.Product, error)
	Get(context.Context, *pbr.PrimaryKeyProduct) (*pbr.Product, error)
	GetList(context.Context, *pbr.ProductRequest) (*pbr.PoroductResponse, error)
	Update(context.Context, *pbr.Product) (*pbr.Product, error)
	Delete(context.Context, *pbr.PrimaryKeyProduct) (*emptypb.Empty, error)
}
type IIncomeStorage interface {
	Create(context.Context, *pbr.Income) (*pbr.Income, error)
	Get(context.Context, *pbr.PrimaryKeyIncome) (*pbr.Income, error)
	GetList(context.Context, *pbr.IncomeRequest) (*pbr.IncomeResponse, error)
	Update(context.Context, *pbr.Income) (*pbr.Income, error)
	Delete(context.Context, *pbr.PrimaryKeyIncome) (*emptypb.Empty, error)
	Count(context.Context, *emptypb.Empty)(*pbr.PrimaryKeyIncome,error)
	//EndIncome(context.Context, *pbr.PrimaryKeyIncome) (*pbr.Income, error)
}

type IIncomeProductStorage interface {
	Create(context.Context, *pbr.IncomeProduct) (*pbr.IncomeProduct, error)
	Get(context.Context, *pbr.PrimaryKeyIncomeProduct) (*pbr.IncomeProduct, error)
	GetList(context.Context, *pbr.IncomeProductRequest) (*pbr.IncomeProductResponse, error)
	Update(context.Context, *pbr.IncomeProduct) (*pbr.IncomeProduct, error)
	Delete(context.Context, *pbr.PrimaryKeyIncomeProduct) (*emptypb.Empty, error)
	Count(context.Context, *emptypb.Empty)(*pbr.PrimaryKeyIncomeProduct,error)
}
type IStorageStorage interface {
	Create(context.Context, *pbr.CreateStorage) (*pbr.Storage, error)
	Get(context.Context, *pbr.PrimaryKeyStorage) (*pbr.Storage, error)
	GetList(context.Context, *pbr.StorageRequest) (*pbr.StorageResponse, error)
	Update(context.Context, *pbr.Storage) (*pbr.Storage, error)
	Delete(context.Context, *pbr.PrimaryKeyStorage) (*emptypb.Empty, error)
}
