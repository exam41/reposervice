
CREATE TYPE "status_enum" AS ENUM (
  'in_process',
  'finished'
);

CREATE TABLE "categories" (
  "id" uuid PRIMARY KEY,
  "name" varchar(100),
  "parent_id" uuid,
  "searching_column" varchar(100),
  "created_at" timestamp DEFAULT 'now()',
  "updated_at" timestamp DEFAULT 'now()',
  "deleted_at" int DEFAULT 0
);

CREATE TABLE "products" (
  "id" uuid PRIMARY KEY,
  "name" varchar(100),
  "category_id" uuid,
  "barcode" varchar(10) UNIQUE,
  "price" int,
  "searching_column" varchar(100),
  "created_at" timestamp DEFAULT 'now()',
  "updated_at" timestamp DEFAULT 'now()',
  "deleted_at" int DEFAULT 0
);


CREATE TABLE "incomes" (
  "id" varchar(20) PRIMARY KEY,
  "courier_id" uuid,
  "status" status_enum,
  "searching_column" varchar(100),
  "created_at" timestamp DEFAULT 'now()',
  "updated_at" timestamp DEFAULT 'now()',
  "deleted_at" int DEFAULT 0
);

CREATE TABLE "income_products" (
  "id" uuid PRIMARY KEY,
  "income_id" varchar(10),
  "category_id" uuid,
  "product_id" uuid,
  "product_name" varchar(100),
  "barcode" varchar(10) UNIQUE,
  "quantity" int,
  "income_price" int,
  "searching_column" varchar(100),
  "created_at" timestamp DEFAULT 'now()',
  "updated_at" timestamp DEFAULT 'now()',
  "deleted_at" int DEFAULT 0
);

CREATE TABLE "storage" (
  "id" uuid PRIMARY KEY,
  "sale_point_id" varchar(10),
  "product_id" uuid,
  "barcode" varchar(10) UNIQUE,
  "income_price" int,
  "quantity" int,
  "total_price" int,
  "searching_column" varchar(100),
  "created_at" timestamp DEFAULT 'now()',
  "updated_at" timestamp DEFAULT 'now()',
  "deleted_at" int DEFAULT 0
);


ALTER TABLE "categories" ADD FOREIGN KEY ("parent_id") REFERENCES "categories" ("id");

ALTER TABLE "products" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");

ALTER TABLE "income_products" ADD FOREIGN KEY ("income_id") REFERENCES "incomes" ("id");

ALTER TABLE "income_products" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");

ALTER TABLE "income_products" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("id");

ALTER TABLE "storage" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("id");