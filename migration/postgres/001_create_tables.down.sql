
drop type if exists "status_enum";

drop table if exists "categories";
drop table if exists "products";
drop table if exists "incomes";
drop table if exists "income_products";
drop table if exists "storage";